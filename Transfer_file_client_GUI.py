# Yanzhi Wang
# 1001827416
# We will import the tkinter module
import tkinter
from tkinter import messagebox

# We will import the socket module
import socket

# We will import the sys module
import sys

# We will import the os module
import os

from _thread import *

# The code regarding the TCP socket programming is learned from the textbook 2.7.2
# We will set the server name to localhost
server_name = "localhost"
# We will arbitrarily choose 13000 for the server port number
server_port = 13000
# We will create the client’s socket
# AF_INET indicates that it is using IPv4
# SOCK_STREAM indicates that the socket is the TCP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# The TCP connection must first be established between the client and server
# So we will perform the three way handshake and
# initiate the TCP connection between the client and server
client_socket.connect((server_name, server_port))

# We will store the content of the queue
word_list = []

send_user_info = ""


# Create write_into_queue_button
# The user enter the word and click the write into queue button,
# the word will get into word_list and the client will print the word_list
def write_into_queue_button():
    """
    The user enter the word and click the write into queue button,
    the word will get into word_list and the client will print the word_list
    """
    # The client will store the user input to the word
    word = enter_queue_content.get()
    # Place the entered text into a queue. There should not be an upper bound on the size of the queue
    # We will append the string to list
    word_list.append(word)
    # The GUI will show notification
    queue_content_notification.config(text="The queue content: " + str(word_list))


# You can sign in and connect to the server if user is not duplicated,
# send file to server, and
# receive the modified file
def signin_button(arg=None):
    """
    You can sign in and connect to the server if user is not duplicated,
    send file to server, and
    receive the modified file
    """
    # If the user input info in the entry, we will store it to the send_user_info
    global send_user_info
    send_user_info = enter_user.get()

    # We will send the info through the client’s socket and into the TCP connection
    global client_socket
    encode_user_info_to_bytes = send_user_info.encode()
    client_socket.send(encode_user_info_to_bytes)

    # When the client is connected,
    # the user should be notified of the active connection
    # The client will receive the info and
    # we will print the receive_info on the user’s display
    # The first thing is that the client will wait data from server
    # We will store the data into receive_info
    receive_user_info = client_socket.recv(1024)
    # The second thing is that we will decode the data
    decode_info_to_string = receive_user_info.decode()
    # The third thing is that we will print the info on the user’s display
    client_status_notification.config(text=decode_info_to_string)
    enter_user.delete(0, tkinter.END)

    # If the provided username is already in use,
    # the client should disconnect and prompt the user to input a different username
    user_is_not_duplicated = (decode_info_to_string == "The user is not duplicated. "
                                                       "You are connecting to the server. ")
    if not user_is_not_duplicated:
        client_socket.close()
        tkinter.messagebox.showerror(title="Duplicated User",
                                     message="Restart the app and enter the different user. ")
        sys.exit(0)

    # Remove the label and button on the GUI
    client_user.pack_forget()
    enter_user.pack_forget()
    signin.pack_forget()
    quit_client.pack_forget()

    # Add label and entry
    # Create the label
    file_name_label = tkinter.Label(root, text="What file you want to transfer? ")
    file_name_label.pack(fill=tkinter.X)

    # Create the entry
    file_name_entry = tkinter.Entry(root)
    file_name_entry.pack(fill=tkinter.X)

    # Create the send file button
    # You can send file to server, and receive the modified file
    def send_file_button():
        """
        You can send file to server, and receive the modified file
        """
        # get file name to send
        f_send = file_name_entry.get()
        # send the text file
        # http://michaldul.com/python/sendfile/
        # What I have learned
        # How to use the sendfile to transfer file
        # It is the first way that I learned regarding how to send file
        global client_socket
        with open(f_send, 'rb') as f:
            client_socket.sendfile(f, 0)

        # https://www.reddit.com/r/learnpython/comments/6z2x97/socket_python_3_file_transfer_over_tcp/
        # This code is working, but I prefer to use the sendfile
        # What I have learned
        # It is the second way that I learned regarding how to send file
        # # open file
        # with open(f_send, "rb") as f_send_to_server:
        #     # send file
        #     data = f_send_to_server.read()
        #     client_socket.sendall(data)

        # Upload the user supplied text file to the server and notify the user of upload completion
        tkinter.messagebox.showinfo(title="Receive Status",
                                    message=client_socket.recv(1024).decode())

        # We will receive the modified file
        # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server
        # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server/blob/master/client.py
        # What I have learned
        # It is the second way that I learned regarding how to receive file
        with open("modified_file_received_at_client_" + str(send_user_info) + ".txt", 'wb') as file:
            while 1:
                data = client_socket.recv(1024)
                if not data:
                    break
                file.write(data)

        # Receive the updated text file from the server and
        # notify the user that the spell check sequence has completed
        # The first thing is to check if there exist the text file in the server
        # The second thing is to check if the text file is empty or not
        # This code was taken from the website
        # Source code website:
        # https://www.guru99.com/python-check-if-file-exists.html
        # What I have learned
        # I learned how to check if the file exist
        if os.path.exists("modified_file_received_at_client_" + str(send_user_info) + ".txt"):
            # This code was taken from the website
            # Source code website:
            # https://stackoverflow.com/questions/2507808/how-to-check-whether-a-file-is-empty-or-not
            # Source code:
            # import os
            # print(os.stat("file").st_size > 0)
            # print(os.path.getsize("file") > 0)
            # What I have learned
            # I learned how to check if the file is empty or not
            if os.stat("modified_file_received_at_client_" + str(send_user_info) + ".txt").st_size > 0:
                tkinter.messagebox.showinfo(title="Process Status",
                                            message="The word process is completed and "
                                                    "the modified file is arrived at the client. ")

    send_file_button = tkinter.Button(root, text="Send file", command=send_file_button)
    send_file_button.pack(fill=tkinter.X)

    # The GUI should provide a way to kill the process without using the ‘exit’ button on the window
    # Create the quit button
    # This code was taken from the website
    # Source code website:
    # https://www.delftstack.com/howto/python-tkinter/how-to-close-a-tkinter-window-with-a-button/
    # What I have learned
    # How to close the GUI with the button
    quit_send_file = tkinter.Button(root, text="Quit the client", command=quit_button)
    quit_send_file.pack(fill=tkinter.X)


# The client will send the queue content to the server
def update_lexicon():
    """
    The client will send the queue content to the server
    """
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # We will set the server name to localhost
    server_name_lexicon = "localhost"
    # We will arbitrarily choose 16000 for the server port number
    server_port_lexicon = 16000
    # We will create the client’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    client_socket_lexicon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # The TCP connection must first be established between the client and server
    # So we will perform the three way handshake and
    # initiate the TCP connection between the client and server
    client_socket_lexicon.connect((server_name_lexicon, server_port_lexicon))

    # The client notify the server that it will get started writing into lexicon
    client_socket_lexicon.send("get started writing into lexicon".encode())
    while True:
        # The client will print Wait for 1 min in the console
        print(client_socket_lexicon.recv(1024).decode())
        # The client will print The server will get the word at the client in the console
        print(client_socket_lexicon.recv(1024).decode())
        # We will convert list to string
        # send string to the server
        # clear list
        # notify the user what is in the list
        add_word_to_lexicon = ""

        # if the list is not empty, we will send everything in list to the server
        if len(word_list) > 0:
            # We will convert list to string
            # This code was taken from the website
            # Source code website:
            # https://www.geeksforgeeks.org/python-program-to-convert-a-list-to-string/
            add_word_to_lexicon = ' '.join([str(elem) for elem in word_list])
            # We will add the space in front of the string
            # The space is there to split the existing content and the new content in the lexicon.txt
            add_word_to_lexicon = " " + add_word_to_lexicon
            # send string to the server
            client_socket_lexicon.send(add_word_to_lexicon.encode())
            # After polling, the client should clear the contents of the queue
            # Once the client has been polled, the contents of the queue should be purged
            # Client purges contents of queue after polling
            # clear list
            word_list.clear()
            # When polled by the server, the client should indicate that a poll was
            # received and print the contents of the
            # queue retrieved by the server
            # Clients indicate they have been polled
            # Client prints contents of queue retrieved by server to GUI
            # Clients notify the user what is in the list and what is retrieved by the server
            queue_content_notification.config(text="It has been polled. The queue content: " +
                                                   str(word_list) +
                                                   " and the content retrieved by the server: " +
                                                   str(add_word_to_lexicon))
        # if the list is empty, we will notify the server that the queue is empty
        else:
            # send string to the server
            client_socket_lexicon.send("The queue is empty".encode())
            # # Once the client has been polled, the contents of the queue should be purged
            # # clear list
            # word_list.clear()
            # notify the user what is in the list
            queue_content_notification.config(text="The queue is empty. It has not been polled. ")
    client_socket_lexicon.close()


# The client is going to notify the server which user info should be deleted
def quit_delete():
    """
    The client is going to notify the server which user info should be deleted
    """
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # We will set the server name to localhost
    server_name_quit = "localhost"
    # We will arbitrarily choose 25000 for the server port number
    server_port_quit = 25000
    # We will create the client’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    client_socket_quit = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # The TCP connection must first be established between the client and server
    # So we will perform the three way handshake and
    # initiate the TCP connection between the client and server
    client_socket_quit.connect((server_name_quit, server_port_quit))

    client_socket_quit.send(send_user_info.encode())
    client_socket_quit.close()


# The client is going to request to delete the user info and quit the GUI
def quit_button():
    """
    The client is going to request to delete the user info and quit the GUI
    """
    start_new_thread(quit_delete, ())
    root.destroy()


# I discussed it with the professor
# The professor relied that
# The mechanism to add elements
# to the queue needs to be present
# on the GUI whenever the program is running
# so the thread is there if the client get started running
start_new_thread(update_lexicon, ())

root = tkinter.Tk()

# The client GUI title is Word Checking Client
root.title('Word Checking Client')
# The client GUI size is 500 x 500
root.geometry("500x500")

# We will create label, entry, and button to let user sign in and quit
# This code was taken from the website
# Source code website:
# https://www.python-course.eu/tkinter_labels.php
# https://www.tutorialspoint.com/python/tk_entry.htm
# What I have learned
# How to create label, entry, and button


# The client will indicate the client status
client_status = tkinter.Label(root, text="The client connection status: ")
client_status.pack(fill=tkinter.X)
client_status_notification = tkinter.Label(root, text="You should login to connect the server. ")
client_status_notification.pack(fill=tkinter.X)

# The client will print the queue content
queue_content = tkinter.Label(root, text="The queue status: ")
queue_content.pack(fill=tkinter.X)
queue_content_notification = tkinter.Label(root, text="The queue content: " + str(word_list))
queue_content_notification.pack(fill=tkinter.X)

# Present the user with the ability to enter additions to the lexicon via the GUI
# Create the entry for queue content
enter_queue_content = tkinter.Entry(root)
enter_queue_content.pack(fill=tkinter.X)

# Present the user with the ability to enter additions to the lexicon via the GUI
# Place the entered text into a queue. There should not be an upper bound on the size of the queue
# Create the write_into_queue button
update_queue = tkinter.Button(root, text="Write into queue", command=write_into_queue_button)
update_queue.pack(fill=tkinter.X)

# Create the label
client_user = tkinter.Label(root, text="The client’s user is: ")
client_user.pack(fill=tkinter.X)

# Create the entry
enter_user = tkinter.Entry(root)
enter_user.pack(fill=tkinter.X)

# Create the sign in button
# This code was taken from the website
# Source code website:
# https://yagisanatode.com/2018/02/26/how-to-display-and-entry-in-a-label-tkinter-python-3/
# What I have learned
# How to get the result from Entry and return it to the Label
signin = tkinter.Button(root, text="Sign in", command=signin_button)
signin.pack(fill=tkinter.X)

# The GUI should provide a way to kill the process without using the ‘exit’ button on the window
# Create the quit button
# This code was taken from the website
# Source code website:
# https://www.delftstack.com/howto/python-tkinter/how-to-close-a-tkinter-window-with-a-button/
# What I have learned
# How to close the GUI with the button
quit_client = tkinter.Button(root, text="Quit the client", command=quit_button)
quit_client.pack(fill=tkinter.X)

root.mainloop()
